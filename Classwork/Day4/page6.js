function Person(name,address,age){
    this.name=name
    this.address=address
    this.age=age
}

Person.prototype.toString=function(){
    return `Person{name:${this.name},address:${this.address},age:${this.age}}`
}

const p1=new Person('Nazim','Pune',24)

p1.toString=function(){
    return `Person{name:${this.name},address:${this.address},age:${this.age}}`
}
console.log(`p1=${p1}`)

const p2=new Person('Akash','Satara',22)
console.log(`p2=${p2}`)