function Person(name,address,age){
    this.name=name
    this.address=address
    this.age=age
}

Person.prototype.canVote=function(){
    if(this.age>18)
    {
        console.log(`${this.name} is eligible to vote`)
    }
    else{
        console.log(`${this.name} is not eligible to vote`)
    }
}
Person.prototype.printInfo=function(){
    console.log(`name:${this.name}`)
    console.log(`address:${this.address}`)
    console.log(`age:${this.age}`)
}

const p1=new Person('Nazim','Pune',24)
const p2=new Person('Akash','Sangali',17)
const p3=new Person('Gopal','Satara',18)

p1.canVote()
p2.canVote()
p3.canVote()
