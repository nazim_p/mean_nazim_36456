const os = require('os')

// console.log(os)

console.log(`platform: ${os.platform()}`)
console.log(`architecture: ${os.arch()}`)

console.log('--- cpu info ---')
const cpus = os.cpus()
for (const cpu of cpus) {
  console.log(cpu)
}
console.log('--- cpu info ---')