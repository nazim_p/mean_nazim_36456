function Laptop(model,company,price){
    this.company=company
    this.model=model
    this.price=price
}

const l1=new Laptop('Dell','Vostro',3500)

l1.printInfo=function(){
    console.log(`Company:${this.company}`)
    console.log(`model:${this.model}`)
    console.log(`price:${this.price}`)
}
l1.printInfo()
console.log(l1)



const l2=new Laptop('Sony','Vaio',3500)

l2.printInfo=function(){
    console.log(`Company:${this.company}`)
    console.log(`model:${this.model}`)
    console.log(`price:${this.price}`)
}
l2.printInfo()
console.log(l2)
