function Laptop(model,company,price){
    this.company=company
    this.model=model
    this.price=price
}

Laptop.prototype.printInfo=function(){
    console.log(`company:${this.company}`)
    console.log(`model:${this.model}`)
    console.log(`price:${this.price}`)
}
const l1=new Laptop('Dell','Vostro',3500)
const l2=new Laptop('Sony','Vaio',3500)

l1.printInfo()
l2.printInfo()

console.log(l1)
console.log(l2)