const person1=new Object()

person1.name="Nazim"
person1.address="Jalna"
person1.age=24

console.log(`name:${person1.name}`)
console.log(`address:${person1.address}`)
console.log(`age:${person1.age}`)

console.log(`------------------------`)

const person2=new Object()
person2[`name`]="Akash"
person2[`address`]="Pune"
person2[`age`]=23

console.log(`name:${person2[`name`]}`)
console.log(`address:${person2[`address`]}`)
console.log(`age:${person2[`age`]}`)
