function function1() {
    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  
    const squares = []
    for (let index = 0; index < numbers.length; index++) {
      const number = numbers[index];
      squares.push(number * number)
    }
  
    console.log(numbers)
    console.log(squares)
  }
  
  // function1()
  
  function function2() {
    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  
    const squares = []
    numbers.forEach(number => {
      squares.push(number * number)
    });
  
    console.log(numbers)
    console.log(squares)
  }
  
  // function2()
  
  function function3() {
    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    const squares = numbers.map(number => number * number)
  
    console.log(numbers)
    console.log(squares)
  }
  
  // function3()
  
  
  function function4() {
    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  
    const cubes = numbers.map(number => number * number * number)
    console.log(numbers)
    console.log(cubes)
  }
  
  // function4()
  
  
  function function5() {
    const temperaturesC = [30, 31, 34, 35, 37, 38]
  

    const temperaturesF = temperaturesC.map(t => (t * (9/5)) + 32)
  
    console.log(temperaturesC)
    console.log(temperaturesF)
  }
  
  // function5()
  
  function function6() {
    const cars = [
      { id: 1, model: 'i20', company: 'hyundai', price: 7.5 },
      { id: 2, model: 'i10', company: 'hyundai', price: 5.5 },
      { id: 3, model: 'fabia', company: 'skoda', price: 6.5 },
      { id: 4, model: 'nano', company: 'tata', price: 2.5 },
      { id: 5, model: 'X5', company: 'BMW', price: 40.0 },
      { id: 6, model: 'Autobiography', company: 'Ranag Rover', price: 95.5 }
    ]
  
    const newCars = []
    for (let index = 0; index < cars.length; index++) {
      const car = cars[index];
  
      
      newCars.push({
        model: car['model'],
        company: car['company']
      })
    }
  
    // console.log(cars)
    console.log(newCars)
  }
  
  // function6()
  
  
  function function7() {
    const cars = [
      { id: 1, model: 'i20', company: 'hyundai', price: 7.5 },
      { id: 2, model: 'i10', company: 'hyundai', price: 5.5 },
      { id: 3, model: 'fabia', company: 'skoda', price: 6.5 },
      { id: 4, model: 'nano', company: 'tata', price: 2.5 },
      { id: 5, model: 'X5', company: 'BMW', price: 40.0 },
      { id: 6, model: 'Autobiography', company: 'Ranag Rover', price: 95.5 }
    ]
  
    const newCars = []
    cars.forEach(car => {
  
      
      newCars.push({
        model: car['model'],
        company: car['company']
      })
    });
  
    console.log(newCars)
  }
  
  // function7()
  
  
  function function8() {
    const cars = [
      { id: 1, model: 'i20', company: 'hyundai', price: 7.5 },
      { id: 2, model: 'i10', company: 'hyundai', price: 5.5 },
      { id: 3, model: 'fabia', company: 'skoda', price: 6.5 },
      { id: 4, model: 'nano', company: 'tata', price: 2.5 },
      { id: 5, model: 'X5', company: 'BMW', price: 40.0 },
      { id: 6, model: 'Autobiography', company: 'Ranag Rover', price: 95.5 }
    ]
  
  
    const newCars = cars.map(car => {
      return { model: car['model'], company: car['company'] }
    })
  
    console.log(newCars)
  }
  
  function8()
  
  
  