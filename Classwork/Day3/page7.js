function Person(name,address,age){
    this.name=name
    this.address=address
    this.age=age
}
const p1= new Person('nazim','pune',24)
console.log(p1)
const p2= new Person('akash','Satara',23)
console.log(p2)
const p3= new Person('Vijay','Karad',22)
console.log(p3)

function Mobile(model,company,price){
    this.model=model
    this.company=company
    this.price=price
}
const m1=new Mobile("Nano","TATA",150000)
console.log(m1)