const { request, response } = require('express')
const express=require('express')
const app=express()
function log1(request,response,next){
    console.log('inside log1')
    next()
}

function log2(request,response,next){
    console.log('inside log2')
    next()
}
const log3 = function (request,response,next){
    console.log('inside log3')
    next()
}

const log4 = (request,response,next)=>{
    console.log('inside log4')
    next()
}
app.use(log1)
app.use(log2)
app.use(log3)
app.use(log4)
app.use((request,response,next)=>{
    console.log('inside anonymous arrow funct')
    next()
})

app.get('/', (request, response) => {
    console.log('inside the route function')
    response.end('test GET /')
  })
  app.get('/product',(request,response)=>{
      const products=[
          {id:1,name:'product1',price:500},
          {id:2,name:'product2',price:600},
          {id:3,name:'product3',price:700}
      ] 
      response.setHeader('Content-type', 'application/json')

         const strProducts= JSON.stringify(products)  
          response.end(strProducts)
  })

  
app.listen(4000,'0.0.0.0',()=>{
    console.log('server started on port no 4000')
})
