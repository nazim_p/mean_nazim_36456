const express = require('express')

const userRouter = require('./routes/user')
const productRouter = require('./routes/product')
const orderRouter = require('./routes/order')


const app = express()

app.get('/', (request, response) => {
  response.end('this is a GET /')
})


app.use(userRouter)
app.use(productRouter)
app.use(orderRouter)

app.listen(4000, '0.0.0.0', () => {
  console.log('server started on port 4000')
})

