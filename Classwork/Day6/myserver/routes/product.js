const { Router } = require('express')
const express = require('express')

const router = express.Router()

router.get('/product', (request, response) => {
  response.end('this is a GET /product')
})

module.exports = router
