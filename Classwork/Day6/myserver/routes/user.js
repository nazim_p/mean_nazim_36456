const express = require('express')

const router = express.Router()

router.get('/user', (request, response) => {
  response.end('this is a GET /user')
})

module.exports = router
