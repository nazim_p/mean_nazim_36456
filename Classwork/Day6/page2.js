const express=require('express')

const app=express()
app.get('/',(request,responce)=>{
    responce.end('this is GET /')
})
app.post('/',(request,responce)=>{
    responce.end('this is POST /')
})
app.put('/',(request,responce)=>{
    responce.end('this is PUT /')
})
app.delete('/',(request,responce)=>{
    responce.end('this is DELETE /')
})

app.get('/product',(request,responce)=>{
    console.log('select * from ..')
    responce.end('this is GET /')
})
app.post('/product',(request,responce)=>{
    console.log('insert into ..')
    responce.end('this is POST /')
})
app.put('/product',(request,responce)=>{
    console.log('update into..')
    responce.end('this is PUT /')
})
app.delete('/product',(request,responce)=>{
    responce.end('this is DELETE /')
    console.log('delete from...')
})


app.listen(4000,'0.0.0.0',()=>{
    console.log('application is litstening to port no 4000')
})