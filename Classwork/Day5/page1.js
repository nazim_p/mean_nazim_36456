const fs=require("fs")

function readSynchronous(){
    console.log(`read file started`)
    try{
    const data=fs.readFileSync("./myfile1.txt")
    console.log(`reading file finished`)
    console.log(`data=${data}`)
    }catch(ex){
        console.log(`exception handled:${ex}`)
    }
    console.log("start calculating multiplication")
    const mult=555*234
    console.log("finished multiplication")
    console.log(`Multiplication=${mult}`)

}
//readSynchronous()

function readAsynchronous(){
    console.log(`read file started`)
    fs.readFile("./myfile1.txt",(error,data)=>{
        console.log(`arrow function called`)
        console.log("reading  file finished")
       
        if(error){
            console.log(`Error:${error}`)
        }
        else{
            console.log(`data=${data}`)
        }
    })
    console.log("start calculating multiplication")
    const mult=555*234
    console.log("finished multiplication")
    console.log(`Multiplication=${mult}`)

}
readAsynchronous()