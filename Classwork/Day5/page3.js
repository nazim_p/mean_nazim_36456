const http = require('http')


const server = http.createServer((request, response) => {
  console.log('a request is received..')

  
 //response.end('<h1 style="color:red">hello from server</h1>')
 response.setHeader('Content-Type','application/jason')
 response.end('{"name":"nazim","address":"pune","age":24}')
})

server.listen(4000, '0.0.0.0', () => {
  console.log('server started successfully on port 4000')
})
