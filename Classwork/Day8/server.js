
const express= require('express')

const bodyParser = require('body-parser')
const routerUser =require('./routes/user')
const routerCategory = require('./routes/category')

const app=express()

app.use(bodyParser.json())

app.get('/',(request,response)=>{
    response.send('<h1>welcome to backend</h1>')
})
app.use('/user',routerUser)

app.use('/category', routerCategory)

app.listen(4500,'0.0.0.0',()=>{
    console.log('server get started on port no 4500')
}) 