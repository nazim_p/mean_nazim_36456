 const mysql=require('mysql2')

 const pool=mysql.createPool({
     host:'localhost',
     user:'root',
     password:'manager',
     port:3306,
     waitForConnections:true,
     connectionLimit:0,
     queueLimit:0
 })
 module.exports={
     connection:pool
 }