const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.get('/', (request, response) => {
  const statement = `select id, title, description from category`
  db.connnection.query(statement, (error, categories) => {
    // create a result using status and data/error keys
    const result = utils.createResult(error, categories)
    response.send(result)
  })
})

router.post('/', (request, response) => {
  const { title, description } = request.body

  const statement = `insert into category (title, description) values ('${title}', '${description}')`
  db.connnection.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

module.exports = router
