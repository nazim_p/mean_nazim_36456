create database mystore;
use mystore;
create table user (id integer primary key auto increment, firstName varchar(20), lastName varchar(20), email varchar(20), password varchar(100), adress varchar(100), phone varchar(100));
create table category (id integer primary key auto_increment,title varchar(100),description varchar(100));
create table product (id integer primary key auto_increment,title varchar(100),description varchar(100),price float,category_id integer);