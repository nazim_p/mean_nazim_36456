
function createResult(error, data) {
    const result = {}
    if (error) {
      // there is an error while executing the query
      result['status'] = 'error'
      result['error'] = error
    } else {
      // the query execution went fine
      result['status'] = 'success'
      result['data'] = data
    }
  
    return result
  }
  
  module.exports = {
    createResult: createResult
  }
  