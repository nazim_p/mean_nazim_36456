const crypto = require('crypto-js')

const password = 'bill'
console.log(`plain password: ${password}`)

console.log(`SHA1 password: ${crypto.SHA1(password)}`)
console.log(`SHA256 password: ${crypto.SHA256(password)}`)
console.log(`SHA512 password: ${crypto.SHA512(password)}`)
console.log(`MD5 password: ${crypto.MD5(password)}`)
