
function function1() {
   
    const numbers = [10, 20, 30, 40, 50]
    console.log(numbers)
    console.log(`type = ${typeof(numbers)}`)

    for (let index = 0; index < numbers.length; index++) {
      console.log(`value at ${index} = ${numbers[index]}`)
    }
  
     console.log('')
  
      for (let number of numbers) {
      console.log(`number = ${number}`)
    }
  
  }
  
   function1()

   function function2() {
   
    const countries = ["india", 'USA', 'UK']
    console.log(countries)
    console.log(`type = ${typeof(countries)}`)
  
  
    for (const country  of countries) {
      console.log(`country = ${country}`)
    }
  }
function2()  

function function3() {
    const numbers = [100, 200, 300, 400, 500]
    console.log(numbers)
  
    // append a value to the end of the collection
    numbers.push(600)
    console.log(numbers)
    numbers.push(700)
    console.log(numbers)

    const lastValue = numbers.pop()
  console.log(`deleted value = ${lastValue}`)
  console.log(numbers)

  numbers.splice(2, 3)
  console.log(numbers)


}
function3()
  
  
  
      