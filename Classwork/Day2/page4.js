function function1() {
    console.log('inside function1')
  }
  function1()

  function function2(p1) {
    console.log('inside function2')
    console.log(`p1 = ${p1}, type = ${typeof(p1)}`)
  }
  //function2(40000)
  //function2("Nazim")
  function2(false)

function add(p1,p2)
{
    console.log(`inside ADD`)
    const add=p1+p2
    console.log(`Addition=${add}`)
}
//add(60,40)
add(20,`30`)


function function3(p1) {
    console.log('inside function3')
    console.log(`p1 = ${p1}, type = ${typeof(p1)}`)
  
    console.log(`arguments = ${arguments}`)
    console.log(arguments)
  }
 // function3(10)
// function3()
 

 function add() {
  console.log(arguments)
  let sum = 0
  for (const number of arguments) {
    sum += number
  }

  console.log(`addition = ${sum}`)
}

//add(10)
add(10, 20)
//add(10, 20, 30)
//add(10, 20, 30, 40)
//add(10, 20, 30, 40, 50)
//add(10, 20, 30, 40, 50, 60, 70, 80, 90, 100)
