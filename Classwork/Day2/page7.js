function function1() {
    const numbers = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
  
    // traditional for loop
    for (let index = 0; index < numbers.length; index++) {
      const number = numbers[index];
      console.log(`number at ${index} = ${number}`)
    }
  
    console.log('')
  
    // for..of loop
    for (const number of numbers) {
      console.log(`number = ${number}`)
    }
  
    console.log('')
  
    // for..each loop
    numbers.forEach((element) => {
      console.log(`element = ${element}, type = ${typeof(element)}`)
    });
  }
  
  // function1()
  
  
  function fuction2(p1) {
    console.log('inside function2')
    console.log(`p1 = ${p1}, type = ${typeof(p1)}`)
    p1()
  }
  
  // fuction2(10)
  // fuction2('test')
  // fuction2(true)
  // fuction2()
  // fuction2(function1)
  
  // fuction2(() => {
  //   console.log('inside arrow function')
  // })
  
  
  function function3() {
  
    // JSON array
    const persons = [
      { name: 'person1', address: 'pune', age: 20 },
      { name: 'person2', address: 'satara', age: 30 },
      { name: 'person3', address: 'mumbai', age: 10 },
      { name: 'person4', address: 'karad', age: 50 }
    ]
  
    persons.forEach(person => {
      console.log(`name: ${person['name']}, address: ${person['address']}, age: ${person['age']}`)
    })
  
  
  
  // function3()
  
  
  function function4() {
    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  
    const squares = []
  
   for (let index = 0; index < numbers.length; index++) {
  const number = numbers[index];
     squares.push(number * number)
   }
  
    
    console.log(numbers)
    console.log(squares)
  }
  
  function4()
