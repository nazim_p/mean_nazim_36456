const express=require('express')
const bodyParser=require('body-parser')
const routerProducts=require('./routes/product')

const app=express()
app.use(bodyParser.json())
app.use(routerProducts)
app.listen(4000,'0.0.0.0',()=>{
    console.log('server started on port no 4000')
})