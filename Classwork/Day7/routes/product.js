
const express=require('express')

const router=express.Router()
 
const products=[
    {id:1,product:'product1',price:100}
]
router.post('/product',(request,response)=>{
   const body=request.body

   const id=body.id
   const product=body.product
   const price=body.price

   products.push({
       id:id,
       product:product,
       price:price
   })
    response.send('product added')
    })
   
router.get('/product',(request,response)=>{

response.send(products)
})/*
router.put('/product',(request,response)=>{
    const body=request.body

    const id=body.productId

    const price =body.price

    for (let index = 0; index < products.length; index++) {
        const product = products[index]
        if(product['id']==id)
        {
            product['price']=price
        }

    }
    response.send('update')
}) */
router.put('/product/:id',(request,response)=>{
    const body=request.body

    const id=request.params.id

    const price =body.price

    for (let index = 0; index < products.length; index++) {
        const product = products[index]
        if(product['id']==id)
        {
            product['price']=price
        }

    }
    response.send('update')
})
// delete
router.delete('/product/:id', (request, response) => {
    const id = request.params.id
    for (let index = 0; index < products.length; index++) {
      const product = products[index];
      if (product.id == id) {
        products.splice(index, 1)
        break
      }
    }
  
    response.send('delete')
  })
  

module.exports=router