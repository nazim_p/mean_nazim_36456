const express = require('express')
const bodyParser = require('body-parser')

const routerProduct = require('./routes/product')

const app = express()
app.use(bodyParser.json())

app.use(routerProduct)

app.listen(4000, '0.0.0.0', () => {
  console.log('server started on port 4000')
})
