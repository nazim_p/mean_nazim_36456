//(A) using JSON

function f1(){
    const Books=[
{Book_Name:'Chava',ISBN_No:111,Author:'Shivaji Savant',Price:700,Category:'Historic'},
{Book_Name:'Wings of Fire',ISBN_No:222,Author:'Arun Tiwari',Price:300,Category:'Autobiography'},
{Book_Name:'Revolution 2020',ISBN_No:333,Author:'Chetan Bhagat',Price:400,Category:'Fiction'},
{Book_Name:'The Great Indian Novel',ISBN_No:444,Author:'Shashi Tharoor',Price:500,Category:'Political'}
    ]
const affordableBook=Books.filter(book=>{
    return book[`Price`]<500
})
console.log(affordableBook)
}
f1()
console.log(`-----------------------------------`)

//using Object

const Books=new Object()
Book.Book_Name="Shyamachi aai"
Book.Author="Sane Guruji"
Book.Price=350;
Book.Category="Education"

console.log(`Book Name  :   ${Book['Book_Name']}`)
console.log(`Book Author  :   ${Book['Author']}`)
console.log(`Book Price  :   ${Book['Price']}`)
console.log(`Book Category  :   ${Book['Category']}`)


  console.log(`----------------------`)

  
  function Book(Book_Name, ISBN_No, Author,Price,Category) {
    this.Book_Name = Book_Name
    this.ISBN_No = ISBN_No  
    this.Author = Author
    this.Price = Price
    this.Category = Category
  }
  
  const book = new Book('Ek Hota Carver', 666, 'Veena Gavankar',750,'Non-Fiction')
  console.log(book)
